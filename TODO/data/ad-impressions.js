window.YTD.ad_impressions.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "The Cybersmile Foundation",
            "screenName" : "@CybersmileHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNICEF"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          } ],
          "impressionTime" : "2020-06-24 01:00:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275135887354089474",
            "tweetText" : "Chokes are EMC components that share aspects with inductors and transformers. Join our June 24 #webinar at 9 A.M. EDT/3 P.M. CEST to explore the construction and characteristics of different types of chokes: https://t.co/KLltIkHKrJ #KEMETishere for #engineers #engineering #tech https://t.co/ghSYh8hZ5w",
            "urls" : [ "https://t.co/KLltIkHKrJ" ],
            "mediaUrls" : [ "https://t.co/ghSYh8hZ5w" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "KEMET Electronics",
            "screenName" : "@KEMETCapacitors"
          },
          "impressionTime" : "2020-06-24 01:01:03"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Android",
            "deviceId" : "JgBihTt9AJY/IOC1OE+ZBFGAoCCQZC8exjS86CcDygs=",
            "deviceType" : "Samsung Galaxy Note III"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273598258934415360",
            "tweetText" : "Kopf versus Bauch – das sind die 4 häufigsten psychologischen Fallen beim Anlegen:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 01:54:06"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CiscoSecure"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "microsoft"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-30 14:39:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194322841948161",
            "tweetText" : "Einfache Firmengründung trotz schwieriger Zeiten? Mit unserem kostenlosen Starterkit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "CEO"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "KMU"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-30 14:39:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273598258934415360",
            "tweetText" : "Kopf versus Bauch – das sind die 4 häufigsten psychologischen Fallen beim Anlegen:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-30 15:03:00"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1184122352755453952",
            "tweetText" : "Datadog's Logging Without Limits™ allows you to cost-effectively collect and manage all your logs. No longer worry about leaving behind logs that matter - ingest them all while keeping your costs under control. Learn more:  https://t.co/MnoLgq4YuK",
            "urls" : [ "https://t.co/MnoLgq4YuK" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Open source"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "List",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 11:16:29"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:44:41"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "commodity"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:10:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1267812108739067907",
            "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bank CIC",
            "screenName" : "@Bank_CIC"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:29:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:28:03"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288793036881891330",
            "tweetText" : "Smarter Helfer für zuhause – mit der Swisscom Home App lässt sich die WLAN-Verbindung des Routers individuell terminieren. So kann bspw. auch Strom eingespart werden.\nMehr dazu: https://t.co/nlfJSXoHz1 https://t.co/ExxStgkoeK",
            "urls" : [ "https://t.co/nlfJSXoHz1" ],
            "mediaUrls" : [ "https://t.co/ExxStgkoeK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:10:41"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289131188167290880",
            "tweetText" : "Das #Allzeithoch geknackt, die 2000-Dollar-Marke im Visier und optimistische #Kursziele der Analysten im Rücken – der #Goldpreis kennt derzeit kein Halten mehr. #CAPinside nennt drei Gründe für und drei Gründe gegen eine Fortsetzung des #Gold #Hoehenflugs.\nhttps://t.co/VJLkALsoF6",
            "urls" : [ "https://t.co/VJLkALsoF6" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Investieren"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:28:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286607550868344832",
            "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Entertainment news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "World news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "BBC News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Sports news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:30:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194322841948161",
            "tweetText" : "Einfache Firmengründung trotz schwieriger Zeiten? Mit unserem kostenlosen Starterkit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 11:30:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286319881412108295",
            "tweetText" : "Wir freuen uns, dass Calmer® einigen Menschen mit ihrem Tinnitus hilft. https://t.co/7QVoziEQ8O https://t.co/M0LK9g65E7",
            "urls" : [ "https://t.co/7QVoziEQ8O" ],
            "mediaUrls" : [ "https://t.co/M0LK9g65E7" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Flare Audio",
            "screenName" : "@flareaudio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          } ],
          "impressionTime" : "2020-07-31 11:15:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194322841948161",
            "tweetText" : "Einfache Firmengründung trotz schwieriger Zeiten? Mit unserem kostenlosen Starterkit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 12:30:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276449710325235714",
            "tweetText" : "A fully charged summer. The Xqisit Powerbank for CHF 49",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 12:26:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 12:30:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Siemens Infrastructure",
            "screenName" : "@SiemensInfra"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WebSecurityIT"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 15:10:01"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1268489685761957889",
            "tweetText" : "Vor dem Hintergrund, dass die Welt zögerlich aus dem Covid-19-Lockdown herauskommt, bewertet Jeremy Lawson, Leiter des ASI Research-Instituts, was der globalen Wirtschaft bevorsteht.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Aberdeen Standard Investments CH",
            "screenName" : "@ASInvestmentsCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IMFNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 17:37:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1103589098021965825",
            "tweetText" : "Our new Mind Mapping tool maintains a good balance of simplicity and function and helps you bring your ideas to life. Give it a try and don't hesitate to share your feedback with us! https://t.co/bXOCWRQ6rl https://t.co/TzHWmB4Xnc",
            "urls" : [ "https://t.co/bXOCWRQ6rl" ],
            "mediaUrls" : [ "https://t.co/TzHWmB4Xnc" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 17:54:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288849633419177984",
            "tweetText" : "Big Tech bosses told they have ‘too much power’ https://t.co/8SezYebPyG",
            "urls" : [ "https://t.co/8SezYebPyG" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Financial Times",
            "screenName" : "@FinancialTimes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UKLabour"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 01:32:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288144437630111747",
            "tweetText" : "Apple hat einen Demontage-Roboter für das iPhone entwickelt.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Apple",
            "screenName" : "@Apple"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "naturschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tierschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "land"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tiere"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 01:21:21"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273575423641890821",
            "tweetText" : "New 0800-DEVOPS #16 is out! Security in the #cloud w/\n@shehackspurple, The Value of #DevOps Transformation w/@nicolefv, @jezhumble and @RealGeneKim. A book by \n@schwartz_cio and #Kubernetes design patterns by \n@bibryam! https://t.co/hGKidbj0n4",
            "urls" : [ "https://t.co/hGKidbj0n4" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CROZ",
            "screenName" : "@croz_hr"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Open source"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ThePSF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@codinghorror"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 22:36:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243538440429469700",
            "tweetText" : "Erfahren Sie, wie Sie Neukunden schneller und einfacher einbinden können. \nLaden Sie unseren Onboarding-Leitfaden für MSPs kostenlos herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer reviews"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "it service"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-31 20:42:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 20:05:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1277578242166267904",
            "tweetText" : "Die letzte #EduDay-Aufnahme vom vergangenen Freitag ist da. Sie können sich diese und alle übrigen Sessions noch mal ansehen und die Präsentationen der Speakerinnen &amp; Speaker downloaden. Das waren sie, die #EduDays 2020. Wir hoffen, es hat Ihnen gefallen. #PiLShweiz #MicrosoftEdu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 20:41:19"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 20:48:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 20:06:11"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286607550868344832",
            "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "BBC News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Entertainment news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "World news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Sports news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:09:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273547632942755843",
            "tweetText" : "UBS Zertifikate mit fixen Coupons und bedingtem Kapitalschutz",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS",
            "screenName" : "@UBS"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TIME"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@faznet"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@handelsblatt"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@wiwo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Reuters"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Blacklist_April"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Blacklist-Juni"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Blacklist_September"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Blacklist"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:56:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275768220734160897",
            "tweetText" : "Ein Sommer der vollen Akkus. Die  Xqisit Powerbank für 49.–",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:55:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194322841948161",
            "tweetText" : "Einfache Firmengründung trotz schwieriger Zeiten? Mit unserem kostenlosen Starterkit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:05:46"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Siemens Infrastructure",
            "screenName" : "@SiemensInfra"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WebSecurityIT"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:05:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Little Nightmares II",
            "screenName" : "@LittleNights"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:14:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288158727615520774",
            "tweetText" : "Verfolge, jage und studiere die Wunder des Tierreiches als Naturkundler, der neuen Tätigkeit im Grenzland in Red Dead Online.\n\nErforsche verschiedene Gebiete, spüre legendäre Tiere auf, setze neue Waffen ein und mehr.\n\nJetzt spielen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Rockstar Games",
            "screenName" : "@RockstarGames"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Ubisoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Steam"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Xbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Halo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@bethesda"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@PlayStation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GameSpot"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IGN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BethesdaStudios"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EA"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#modding"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#gamers"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "modding"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "backward compatible"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#streaming"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#gamer"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#nowplaying"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#androidgames"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gamer"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "modded"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#games"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#gaming"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-latam-asia-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-001-rdops4xb1-activeplayers-latam-asia-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-001-rdops4xb1-activeplayers-eu-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-eu-18ce54s7sud"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:05:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288519058845908992",
            "tweetText" : "We built FaunaDB so you can focus on building your app instead of data infrastructure. See what others are saying!\n\nTry FaunaDB: https://t.co/48mbnD2Up2 https://t.co/v7gUcMMR4c",
            "urls" : [ "https://t.co/48mbnD2Up2" ],
            "mediaUrls" : [ "https://t.co/v7gUcMMR4c" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 16:09:32"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283031834281021441",
            "tweetText" : "#KI: Das kann die Technologie bereits - Beispiel #Objekterkennung in Luftbildern: \nhttps://t.co/PTnPzNSkjJ\n#GeoAI #GIS #MachineLearning",
            "urls" : [ "https://t.co/PTnPzNSkjJ" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Esri Deutschland",
            "screenName" : "@Esri_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 16:10:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Little Nightmares II",
            "screenName" : "@LittleNights"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 16:14:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285599885589282823",
            "tweetText" : "Check out 15+ code editors for a range of programming languages and technologies, all available in one app developed by #JetBrains.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "JetBrains",
            "screenName" : "@jetbrains"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ThePSF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledevs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reddit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Microsoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@VisualStudio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#embeddedlinux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#embedded"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#c#embedded"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#firmware"
          }, {
            "targetingType" : "List",
            "targetingValue" : "JetBrains - Twitter followers - 25.02.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Opted-Out"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains_Buyers_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "ToolBox_Evaluators"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Jetbrains_team_visitors"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains employees"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 19:44:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1250468933318164481",
            "tweetText" : "Strengthen your website security with The Ultimate Guide to Securing Client Sites. 💪🏼\n\nDownload our free guide below:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kinsta",
            "screenName" : "@kinsta"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@css"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Purchase"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Affiliates"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers November 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers October 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Current Customers"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 19:43:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282660312953618432",
            "tweetText" : "Die Freude über eigene Kinder ist unbezahlbar. 👨‍👩‍👧 Doch was kostet es, wenn man plötzlich zu dritt ist und wie finanziert man das?",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 19:38:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:43:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:43:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Mary Kuempel",
            "screenName" : "@MaryKuempel"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:44:46"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288141250445672456",
            "tweetText" : "Bei der Endfertigung des iPhone entsteht kein Deponiemüll.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Apple",
            "screenName" : "@Apple"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "tierschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "naturschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "land"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tiere"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 21:44:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:43:40"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285205515442814976",
            "tweetText" : "Tauchen Sie voll digital in die Welt von VMware! IT-Problemstellungen &amp; Lösungen kompakt erklärt. Unsere Experten sind für Sie da.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Oracle"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:45:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 21:43:36"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1204325243432656896",
            "tweetText" : "Create, collaborate, and centralize communication for all your\ncross-functional team work. Try Miro! https://t.co/iP3MTOfZbP https://t.co/gevlg0g46g",
            "urls" : [ "https://t.co/iP3MTOfZbP" ],
            "mediaUrls" : [ "https://t.co/gevlg0g46g" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobileUS"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-31 21:45:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288446178833715200",
            "tweetText" : "📢 Webinar: #JustTransition in the #Aviation Sector\n🗓 4th of August, 4-6 pm CEST\n\nHow can we manage a transition towards a climate-just transport system? And what about the workers and communities dependent on aviation?\n\nMore details and registration: https://t.co/pYGnH7oOOZ https://t.co/XfO2hfu2Hv",
            "urls" : [ "https://t.co/pYGnH7oOOZ" ],
            "mediaUrls" : [ "https://t.co/XfO2hfu2Hv" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Stay Grounded Network",
            "screenName" : "@StayGroundedNet"
          },
          "impressionTime" : "2020-07-31 18:46:03"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287824276134010882",
            "tweetText" : "Öffnet die Portemonnaies, denn über 50 Schweizer Games sind in einem speziellen Steam-Sale! 💸👛\n\nWie's kommt? Das erfahrt ihr hier:\n\nhttps://t.co/E1FJmljtfU",
            "urls" : [ "https://t.co/E1FJmljtfU" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "eSports.ch",
            "screenName" : "@eSportsCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Steam"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "13 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:44:32"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288024072044138503",
            "tweetText" : "Robin Data erhält #Wachstumsfinanzierung durch @bmp_ventures und weitere Business Angel. \n\n@profadoering CEO und Co-Founder der Robin Data GmbH setzt klare Ziele: \"Wir wollen das beste Produkt im wachsenden Markt für digitalen #Datenschutz.\" \n\nhttps://t.co/NwlYddRLBW",
            "urls" : [ "https://t.co/NwlYddRLBW" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Robin Data",
            "screenName" : "@RobinData_DE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          } ],
          "impressionTime" : "2020-07-31 18:45:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275062714025431041",
            "tweetText" : "Auch die Aufnahme des 3. #EduDays ist nun online verfügbar. Sehen Sie sich gerne (nochmal) die Inputs samt Präsentationen von @GromovaKatarina, @SchulerAnita, @Beedle_co und @GoyMichael rund um Erklärvideos, Teams und OneNote an. #MicrosoftEdu #PiLSchweiz",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "One Note"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:45:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283375475931451400",
            "tweetText" : "Der #GhostofTsushima erhebt sich auf #PS4 und stellt sich gegen die mongolischen Besatzer.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PlayStationDE",
            "screenName" : "@PlayStationDE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:45:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288563274313797636",
            "tweetText" : "\"Robins envisioned a broader mission for herself and her fellow writers, not only protesting women’s political disenfranchisement, but countering stereotypes of women with more truthful depictions.\" Read the full blog by Mary Christian.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Palgrave Macmillan",
            "screenName" : "@Palgrave"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel_en"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:45:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275026152055361536",
            "tweetText" : "Pack your swimming trunks and grab your new iPhone 11. Now with a CHF 100 discount!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BillGates"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@elonmusk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SpaceX"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitter"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleSupport"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JeffBezos"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sundarpichai"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppStore"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleMusic"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@iphone_dev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iOS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:45:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276120845790982144",
            "tweetText" : "Bleiben Sie wettbewerbsfähig, beschleunigen Sie Innovationen und senken Sie die Kosten mit Red Hat.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Red Hat Telco",
            "screenName" : "@RedHatTelco"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Swisscom_de"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:44:40"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1284006965912494080",
            "tweetText" : "Niemand kann die Zukunft vorhersagen. Doch mit Derivaten kann man darauf wetten.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-31 18:45:53"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287919300657868800",
            "tweetText" : "Fix cloud misconfigurations for AWS, GCP, Terraform, K8s, and CloudFormation in run-time and build-time automatically.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "bridgecrew",
            "screenName" : "@bridgecrewio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@troyhunt"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 13:40:56"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1152315001371336707",
            "tweetText" : "Meet CrossBox - a brand new Webmail App for cPanel, Plesk, and Direct Admin. It's intuitive, it's fast, and it looks pretty darn good! Choose to do email properly, choose the CrossBox #Webmail #App. https://t.co/5Bxtqyjnk0 https://t.co/TKwxSMEEbM",
            "urls" : [ "https://t.co/5Bxtqyjnk0" ],
            "mediaUrls" : [ "https://t.co/TKwxSMEEbM" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CrossBox",
            "screenName" : "@CrossBoxIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 13:43:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243559139286159366",
            "tweetText" : "Das digital gestützte Recruiting 4.0 ist die Abkürzung zu den besten Kandidaten. Erfahren Sie im Whitepaper, wie datengetriebene Ansätze und maßgeschneidertes Bewerbermanagement einen komplett neuen Rekrutierungsprozess ermöglichen. ✓ Jetzt lesen!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "EASY SOFTWARE",
            "screenName" : "@easy_software"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:30:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288519058845908992",
            "tweetText" : "We built FaunaDB so you can focus on building your app instead of data infrastructure. See what others are saying!\n\nTry FaunaDB: https://t.co/48mbnD2Up2 https://t.co/v7gUcMMR4c",
            "urls" : [ "https://t.co/48mbnD2Up2" ],
            "mediaUrls" : [ "https://t.co/v7gUcMMR4c" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "zeit"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "JAMstack"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 11:37:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289339271829024769",
            "tweetText" : "Your agency may have taken off, but that doesn't mean your client site issues have to grow. Learn how to best manage multiple WordPress sites with our free guide: 📕",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kinsta",
            "screenName" : "@kinsta"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@css"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Purchase"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Affiliates"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers November 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers October 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Current Customers"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 11:40:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288019071599104000",
            "tweetText" : "The weather script customized by our tec team allows us to prominently position Post’s ads for its hiking campaign on Google exactly on the days when it counts the most – during perfect hiking weather.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Webrepublic",
            "screenName" : "@webrepublic"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Marketing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#contentmarketing"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276442007255252992",
            "tweetText" : "The perfect summer accessories: Samsung Galaxy buds for CHF 99",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:40:41"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282665929122500610",
            "tweetText" : "🌴☀ Einmal im Leben eine Weltreise machen! Was gilt es zu beachten bei Planung und Visa, Impfungen und Versicherungen, AHV und Pensionskasse? Wir erklären's:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:37:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287653232899248129",
            "tweetText" : "#Erwartungen, #Prognosen und #Hoffnungen sind wichtiger als vergangene #Quartalszahlen oder aktuelle #Wirtschaftsdaten. So unsicher wie heute war die Zukunft wohl selten, trotzdem läuft es an den #Aktienmaerkten wieder sehr gut. Zu gut vielleicht? \nhttps://t.co/AE3MGtrWog",
            "urls" : [ "https://t.co/AE3MGtrWog" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:40:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289427694581510144",
            "tweetText" : "Weshalb habt ihr ein #Freizügigkeitskonto eröffnet?",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Vorsorgewissen.info",
            "screenName" : "@vorsorgewissen"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:13"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194328911028224",
            "tweetText" : "Sie wollen in wenigen Schritten zur eigenen Firma? Unser Alles-drin-Paket macht’s möglich.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Innovation"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287758523695235076",
            "tweetText" : "Swedish companies reap benefits of country’s Covid-19 approach https://t.co/4mSzDZzJTG",
            "urls" : [ "https://t.co/4mSzDZzJTG" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Financial Times",
            "screenName" : "@FinancialTimes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@washingtonpost"
          }, {
            "targetingType" : "List"
          }, {
            "targetingType" : "List"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:32"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276436789440454656",
            "tweetText" : "Das perfekte Sommer-Accessoire: Samsung Galaxy Buds für 99.–",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:01"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287976074668802048",
            "tweetText" : "https://t.co/yqPYa9veMy ist stolz auf seine Internet-Endung .swiss und zeigt sie deutlich auf seiner Website: \nhttps://t.co/hJveT3yXlz",
            "urls" : [ "https://t.co/yqPYa9veMy", "https://t.co/hJveT3yXlz" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "dot.swiss",
            "screenName" : "@mydotswiss"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1284110073111949313",
            "tweetText" : "Was man als Eltern zu \"Battle Passes\" in Online-Games wissen muss.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Michael In Albon, Medienkompetenzexperte",
            "screenName" : "@MichaelInAlbon"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "mutter"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "eltern"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "teaching"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "vater"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:37:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288836385013731328",
            "tweetText" : "This practical course covers the fundamental IoT technologies and their application in various business domains. Participants will learn how to bootstrap IoT projects for real-world use cases, select the underlying platforms, and take the IoT solution from conception to adoption.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Hochschule Luzern",
            "screenName" : "@hslu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Oracle"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Microsoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Swisscom_de"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 11:36:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1280882835545612293",
            "tweetText" : "Spannende halb- und ganztägige Workshops und Keynotes  mit Top-Trainer*innen! Jetzt Ticket für den #SoftwareArchitectureSummit in Berlin oder online sichern und von den Besten lernen!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Softw. Arch. Summit",
            "screenName" : "@SoftwArchSummit"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Frontend"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:32:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286397352794710016",
            "tweetText" : "Learn the latest best practices for SaaS CTOs and technical leaders around leveling up your company's security with this checklist ✅",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Linux"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 11:37:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275768189343932418",
            "tweetText" : "Strahlendes Sommerwetter, gute Laune  &amp; volle Akkus. Die Xqisit Powerbank lässt dich am Strand nie im Stich. Jetzt zuschlagen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194336351719425",
            "tweetText" : "Want to become your own boss in a few steps? It’s possible with our all-you-need package.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "innovation"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "boss"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289091129854644224",
            "tweetText" : "Jetzt anmelden zum kostenlosen #Webinar '#Blazor Server: Mögliche Architekturalternative zu SPAs?' von @PhoenixHawk ⬇️ https://t.co/nxcOY9nJEP",
            "urls" : [ "https://t.co/nxcOY9nJEP" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Thinktecture",
            "screenName" : "@thinktecture"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1250881223687204866",
            "tweetText" : "Bots account for 50% of all web traffic. Today, more than ever, #security experts need improved defenses against bot-generated traffic.\n\nThis ebook reveals the ways attackers use #bots and how to defend against the most common attacks.\n\nGet your copy 👇",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@owasp"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HackingDave"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:37:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288416525309927424",
            "tweetText" : "Risiken gehören zum Auslandsgeschäft. Das weiß jedes Unternehmen, das international tätig ist. Umso wichtiger ist es, sich gegen potentielle Risiken bestmöglich abzusichern. Mehr dazu erfahren Sie auf unserem Firmenkundenportal.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Commerzbank",
            "screenName" : "@commerzbank"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:40:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288826025481314304",
            "tweetText" : "„Orientierung im Sturm der Gefühle“ – unsere Geschäftsführerin Martina Vollbehr war zu Gast im aktuellen FAW-Podcast und hat die „pilot Radar“-Studienreihe vorgestellt. Jetzt anhören &gt;&gt; https://t.co/kpgW96msuC",
            "urls" : [ "https://t.co/kpgW96msuC" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "pilot Agenturgruppe",
            "screenName" : "@pilot_agentur"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Media"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286277474574381056",
            "tweetText" : "Die Einigung der #EU-Staats- und Regierungschefs auf ein #Finanzierungspaket von insgesamt 1,824 Billionen Euro [1] ist ein wichtiges Ergebnis und ein zieht positive kurzfristige #Impulse für #europaeische #Assets nach sich. https://t.co/94dlbcZcvY",
            "urls" : [ "https://t.co/94dlbcZcvY" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:31:11"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285137369570725888",
            "tweetText" : "Setzen Sie auf Fern- oder #Hybridunterricht? Möchten Sie Ihren #Unterricht nach den Ferien nachhaltig auf #BlendedLearning umstellen? Beginnen Sie hier!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "unterricht"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1280497534025240577",
            "tweetText" : "Höher, schneller, weiter … Bis eine gewaltige Krise ALLES infrage stellt. Brisant, brandaktuell – jetzt als Taschenbuch!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Blanvalet Verlag",
            "screenName" : "@BlanvaletVerlag"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@welt"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@derspiegel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@faznet"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SZ"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:39:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1250468933318164481",
            "tweetText" : "Strengthen your website security with The Ultimate Guide to Securing Client Sites. 💪🏼\n\nDownload our free guide below:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kinsta",
            "screenName" : "@kinsta"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@css"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Purchase"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Affiliates"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers November 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers October 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Current Customers"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 11:38:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287758521774354434",
            "tweetText" : "Swedish companies reap benefits of country’s Covid-19 approach https://t.co/OFVTFV4WPB",
            "urls" : [ "https://t.co/OFVTFV4WPB" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Financial Times",
            "screenName" : "@FinancialTimes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UKLabour"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:36:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288465604509413376",
            "tweetText" : "Stress weicht Leichtigkeit im #Corona-Alltag – die zunehmende Positivität der Deutschen lässt auch die Konsumlaune steigen. Weitere Insights und Empfehlungen für die #Kommunikationswirtschaft liefert unsere neueste „pilot Radar“-Ausgabe: https://t.co/5fCG0IF476",
            "urls" : [ "https://t.co/5fCG0IF476" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "pilot Agenturgruppe",
            "screenName" : "@pilot_agentur"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Media"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 11:38:30"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273598258934415360",
            "tweetText" : "Kopf versus Bauch – das sind die 4 häufigsten psychologischen Fallen beim Anlegen:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 22:09:18"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282666263626551298",
            "tweetText" : "🏠  Ihr träumt von einer Ferienwohnung oder einem Ferienhaus am Lieblingsort? Wir zeigen, worauf es beim Kauf ankommt:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 22:10:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282661502651174912",
            "tweetText" : "⛵ Erst mit Mitte 60 in den Ruhestand? Das muss nicht sein. Wie der Traum von der Frühpensionierung wahr wird:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 18:58:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282662087442006016",
            "tweetText" : "🐎 Der Traum vom eigenen Pferd: Wie Ihr ihn Euch erfüllt und welche Kosten auf Euch zukommen 👉 https://t.co/vma2Xea3Du https://t.co/PxsI6Ojxkq",
            "urls" : [ "https://t.co/vma2Xea3Du" ],
            "mediaUrls" : [ "https://t.co/PxsI6Ojxkq" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 17:20:21"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287674086823530498",
            "tweetText" : "Mit unseren Ressourcen können Sie Ihren #Unterricht schnell an die Gegebenheiten beim #Hybridunterricht anpassen. Auch wenn nichts ein echtes #Klassenzimmer und den direkten Kontakt ersetzt, können Tools wie #MSTeams und #Flipgrid entscheidende Lernerfolge ermöglichen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Unterricht"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 17:17:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286223902801264642",
            "tweetText" : "Create, collaborate, and centralize communication for all your\ncross-functional team work. Try Miro. https://t.co/4sk0DEb783 https://t.co/RiPQVUYde8",
            "urls" : [ "https://t.co/4sk0DEb783" ],
            "mediaUrls" : [ "https://t.co/RiPQVUYde8" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobileUS"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 17:31:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1267812108739067907",
            "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bank CIC",
            "screenName" : "@Bank_CIC"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:39:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240512073240641536",
            "tweetText" : "Missing your office whiteboard? Try Miro, the online collaborative whiteboard. Free forever. No credit card required. Sign up and try! https://t.co/LuCmK5Gjwg https://t.co/lMQTjzdCxw",
            "urls" : [ "https://t.co/LuCmK5Gjwg" ],
            "mediaUrls" : [ "https://t.co/lMQTjzdCxw" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Office365"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Office"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business software"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:28:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1284006965912494080",
            "tweetText" : "Niemand kann die Zukunft vorhersagen. Doch mit Derivaten kann man darauf wetten.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276436760281612289",
            "tweetText" : "Die passen in jede Strandtasche, deine Samsung Galaxy Buds.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@verge"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Gizmodo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GooglePlay"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wired"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:31"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1268489685761957889",
            "tweetText" : "Vor dem Hintergrund, dass die Welt zögerlich aus dem Covid-19-Lockdown herauskommt, bewertet Jeremy Lawson, Leiter des ASI Research-Instituts, was der globalen Wirtschaft bevorsteht.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Aberdeen Standard Investments CH",
            "screenName" : "@ASInvestmentsCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IMFNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:38:31"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288051575504740354",
            "tweetText" : "Durch Homeschooling in Coronazeiten musste der Schulunterricht in kürzester Zeit digitalisiert werden. Viele Eltern haben so gemerkt, dass der Lerneffekt ihrer Kinder durch Smartphones und Tablets sogar steigen kann. Hat das herkömmliche Klassenzimmer ausgedient? https://t.co/U2wjIc1dtU",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/U2wjIc1dtU" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Michael In Albon, Medienkompetenzexperte",
            "screenName" : "@MichaelInAlbon"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:40:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288467643285020672",
            "tweetText" : "#Hypotheken bequem im Voraus verlängern: Mit #key4byUBS finden unsere Kunden aus Angeboten verschiedener Anbieter das Passende – das kann auch eine Kombination mehrerer Angebote sein.\nhttps://t.co/PfQ0Oz4J9n https://t.co/Oe3OgfyVUb",
            "urls" : [ "https://t.co/PfQ0Oz4J9n" ],
            "mediaUrls" : [ "https://t.co/Oe3OgfyVUb" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "key4 by UBS",
            "screenName" : "@key4byUBS"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Immobilien"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289186143813042180",
            "tweetText" : "Popular #crypto token performance (24h) @OKEx: https://t.co/ah1EcaWlcs\n\n$BTC/USD: $11214.00 (+2.67%)\n$ETH/USD: $344.03 (+8.56%)\n$OKB/USD: $5.82 (+1.87%)\n$XRP/USD: $0.25 (+2.58%)\n$BCH/USD: $295.05 (+4.45%)\n$EOS/USD: $3.08 (+3.36%)",
            "urls" : [ "https://t.co/ah1EcaWlcs" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "OKEx",
            "screenName" : "@OKEx"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ExpansionMx"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@lajornadaonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Pajaropolitico"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@binance"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Bitcoin cryptocurrency"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Litecoin cryptocurrency"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Ethereum cryptocurrency"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Innovation"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#innovation"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "innovation"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "disruption"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "BTC"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#Innovation"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#disruption"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "btc"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#BTC"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:11:56"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286319881412108295",
            "tweetText" : "Wir freuen uns, dass Calmer® einigen Menschen mit ihrem Tinnitus hilft. https://t.co/7QVoziEQ8O https://t.co/M0LK9g65E7",
            "urls" : [ "https://t.co/7QVoziEQ8O" ],
            "mediaUrls" : [ "https://t.co/M0LK9g65E7" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Flare Audio",
            "screenName" : "@flareaudio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          } ],
          "impressionTime" : "2020-08-01 19:41:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283748572412903429",
            "tweetText" : "Free VPNs based in Hong Kong caught logging\n#VPN #HongKong #privacy https://t.co/4QK8a2HFFm",
            "urls" : [ "https://t.co/4QK8a2HFFm" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Private Internet Access VPN (PIA)",
            "screenName" : "@buyvpnservice"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "privacy"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#privacy"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:12:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282974348639449088",
            "tweetText" : "Da Lehrpersonen teils weiterhin auf Distanz unterrichten werden, spielen #Eltern eine wesentliche Rolle, indem sie den Schülerinnen und Schülern helfen, sich mit den neuen Technologien zurechtzufinden. Klicken Sie sich für einen kleinen Einblick durch diese interaktive Demo:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "unterricht"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:13:35"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283375475931451400",
            "tweetText" : "Der #GhostofTsushima erhebt sich auf #PS4 und stellt sich gegen die mongolischen Besatzer.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PlayStationDE",
            "screenName" : "@PlayStationDE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286203776181796870",
            "tweetText" : "Ryan Milewski ⭐⭐⭐⭐⭐\n\"The online quoting is fast and easy to use and our boards arrived on time and packaged well. \nIt works great for ordering prototype or small batch pcb assembly order from Seeed.\"\n\nLearn More about Seeed PCBA https://t.co/KmM1CJsJCl https://t.co/N8IHi0z4tV",
            "urls" : [ "https://t.co/KmM1CJsJCl" ],
            "mediaUrls" : [ "https://t.co/N8IHi0z4tV" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Seeed Studio",
            "screenName" : "@seeedstudio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hackaday"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@arduino"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:11:14"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1194259127419015168",
            "tweetText" : "How does morning classroom lighting impact alertness and mood in students? Read the article here: https://t.co/mBToPX1PKN. https://t.co/UageVL2KgC",
            "urls" : [ "https://t.co/mBToPX1PKN" ],
            "mediaUrls" : [ "https://t.co/UageVL2KgC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Scientific Reports",
            "screenName" : "@SciReports"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nature"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:40:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289191653039718400",
            "tweetText" : "#SecurityTokenOfferings klingen nach #Tech, #Krypto und #Zukunft. Doch dass die #STOs auch mittelständischen Unternehmen mit einer langen Geschichte helfen können, beweist die #Traditionsreederei #Vogemann. https://t.co/3KHQ8XmtHy",
            "urls" : [ "https://t.co/3KHQ8XmtHy" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275026176243834880",
            "tweetText" : "More sun, more discounts, more for you! The iPhone 11 now only CHF 709.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BillGates"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@elonmusk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SpaceX"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitter"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleSupport"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JeffBezos"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sundarpichai"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppStore"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleMusic"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@iphone_dev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iOS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286654882355847173",
            "tweetText" : "#Governance-Aspekte sollten bei #Investitionsentscheidungen eine größere Rolle spielen als bislang. Das zeigt der Fall #Wirecard sehr deutlich. Der Wert guter #Unternehmensfuehrung lässt sich konkret an einem #Index ablesen. https://t.co/5148phBrcs",
            "urls" : [ "https://t.co/5148phBrcs" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Investieren"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:39:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273298030113034240",
            "tweetText" : "Starte dein NETWAYS Managed Kubernetes und beginne in wenigen Minuten mit Deiner Container-Plattform. Setup, Updates, Betrieb übernehmen wir für Dich. Managed Kubernetes ist wie für Dich gemacht. \nJetzt gleich loslegen!\n✅ https://t.co/wt3kwIsj0M https://t.co/5dQOGU31J6",
            "urls" : [ "https://t.co/wt3kwIsj0M" ],
            "mediaUrls" : [ "https://t.co/5dQOGU31J6" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "NETWAYS Web Services",
            "screenName" : "@NetwaysCloud"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288154436502790145",
            "tweetText" : "SpringOne is two days of breakout sessions, interactive workshops, networking &amp; a few surprises—all focused on getting your apps to production. Register to join us—it's all online &amp; all free 👇 https://t.co/Fta22Yk2Cl",
            "urls" : [ "https://t.co/Fta22Yk2Cl" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "VMware Tanzu",
            "screenName" : "@VMwareTanzu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:19"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1259947143919353856",
            "tweetText" : "Did you know that every 1 second delay in site speed can decrease conversion rates by 7%? Don't let site speed hurt your clients' bottom line - learn how to speed up WordPress sites. ⚡️",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kinsta",
            "screenName" : "@kinsta"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@css"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Purchase"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Affiliates"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers November 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Kinsta Current Customers October 2018"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Current Customers"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:38:49"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288391312681709571",
            "tweetText" : "Die #Coronakrise hat #Volkswirtschaften weltweit in eine tiefe #Wirtschaftskrise gestürzt. Das aktuelle #IMDWorldCompetitivenessRanking zeigt: Vor allem die #skandinavischen Länder sind gut positioniert, um schnell wieder in die Spur zu gelangen. \nhttps://t.co/uhLbcjHSah",
            "urls" : [ "https://t.co/uhLbcjHSah" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Investieren"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283402881048223745",
            "tweetText" : "Already have a PowerShell cheat sheet yet? If not, then we have good news: You can order one for free! The most important cmdlets at a glance – your perfect sidekick for PowerShell scripting #PowerShell #cmdlets #KeepOnScripting https://t.co/vD0aiqzfL4 https://t.co/DZQoAe5zIz",
            "urls" : [ "https://t.co/vD0aiqzfL4" ],
            "mediaUrls" : [ "https://t.co/DZQoAe5zIz" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "ScriptRunner 🤖",
            "screenName" : "@Script_Runner"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:35:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286397296297443328",
            "tweetText" : "Learn the latest best practices for SaaS CTOs and technical leaders around leveling up your company's security with this checklist ✅",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Linux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:42:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287824276134010882",
            "tweetText" : "Öffnet die Portemonnaies, denn über 50 Schweizer Games sind in einem speziellen Steam-Sale! 💸👛\n\nWie's kommt? Das erfahrt ihr hier:\n\nhttps://t.co/E1FJmljtfU",
            "urls" : [ "https://t.co/E1FJmljtfU" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "eSports.ch",
            "screenName" : "@eSportsCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Steam"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "13 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:40:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244028128814456832",
            "tweetText" : "Baby Sitter  c.1935 Japan \n山村の子守 https://t.co/oZ9IfDAQHK",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/oZ9IfDAQHK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kansuke Yamamoto 山本悍右：写真家 シュルレアリスト",
            "screenName" : "@Surrealism_KY"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Arts and crafts"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Photography"
          } ],
          "impressionTime" : "2020-08-01 19:11:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1268513261869305859",
            "tweetText" : "Free Excel tutorial: https://t.co/EZ1ULbjAya https://t.co/9haRv8Kfkp",
            "urls" : [ "https://t.co/EZ1ULbjAya" ],
            "mediaUrls" : [ "https://t.co/9haRv8Kfkp" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Excel Easy",
            "screenName" : "@ExcelEasy"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:12:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1273921654373199872",
            "tweetText" : "Mehr Sonne, mehr Rabatte, mehr für dich! Das iPhone 11 jetzt für 709.–",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BillGates"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@elonmusk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SpaceX"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitter"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleSupport"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JeffBezos"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sundarpichai"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppStore"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleMusic"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@iphone_dev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iOS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1259844569220427782",
            "tweetText" : "#Familybusinesses continue to re-examine their #businessmodels and #strategy as they adapt to #crisis. Advice from experts on what they should be doing now. https://t.co/2jzrBXpHr2 https://t.co/ejxccQIevJ",
            "urls" : [ "https://t.co/2jzrBXpHr2" ],
            "mediaUrls" : [ "https://t.co/ejxccQIevJ" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "EIX.org Social",
            "screenName" : "@EIXSocial"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IMFNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:12:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288864401865441287",
            "tweetText" : "Russia is heading toward a recession, with negative growth projected for most sectors in 2020. What policies can help mitigate the impacts of the #COVID19 pandemic on #Russia’s economy? Learn more: https://t.co/uNZgi0MMl0",
            "urls" : [ "https://t.co/uNZgi0MMl0" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "World Bank Europe and Central Asia",
            "screenName" : "@WorldBankECA"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          } ],
          "impressionTime" : "2020-08-01 19:11:58"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286224008275427328",
            "tweetText" : "Create a hub for cross-functional work that also works with all your other tools. Centralize and standardize communication with Miro. https://t.co/4sk0DEb783 https://t.co/NXt5dWJ1yH",
            "urls" : [ "https://t.co/4sk0DEb783" ],
            "mediaUrls" : [ "https://t.co/NXt5dWJ1yH" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobile"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobileUS"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:41:32"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194322841948161",
            "tweetText" : "Einfache Firmengründung trotz schwieriger Zeiten? Mit unserem kostenlosen Starterkit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:39:03"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288142670444068864",
            "tweetText" : "Apple recycelt dein Smartphone. Auch wenn es kein iPhone ist.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Apple",
            "screenName" : "@Apple"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "land"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tiere"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tierschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "naturschutz"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:42:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288902672242872320",
            "tweetText" : "Mit über 14.000 verwalteten Produktionsclustern ist der IBM Cloud Kubernetes Service eines der führenden Angebote im Bereich Workloadskalierung und -vielfalt sowie Premium-Sicherheit. Erste Schritte zum eigenen kostenlosen Cluster",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "IBM Cloud",
            "screenName" : "@IBMcloud"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@codinghorror"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:49"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1182260407748239361",
            "tweetText" : "JetBrains Rider: a cross-platform .NET IDE. Develop .NET, https://t.co/hnt8mpkOKF, .NET Core, Xamarin or Unity applications on Windows, Mac, or Linux",
            "urls" : [ "https://t.co/hnt8mpkOKF" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "JetBrains",
            "screenName" : "@jetbrains"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@VisualStudio"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Resharper_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "ReSharper_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "TeamCity_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "TeamCity_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "AppCode_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "AppCode_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "ReSharperCPP_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "CPP_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "CLion_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "CLion_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Rider_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "GoLand_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains_Buyers_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "dotTools_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "PhpStorm_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "YouTrack_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "WebStorm_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "PhpStorm_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Help_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Support_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "YouTrack_Download_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "YouTrack_InCloud_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "PyCharm_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "PyCharm_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RubyMine_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RubyMine_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Upsource_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Upsource_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "IDEA_Visitors_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "IDEA_Evaluators_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "pycharm_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "phpstorm_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrains_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "intellijidea_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "WebStormIDE_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "teamcity_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rubymine_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "resharper_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "resharper_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "intellijidea_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "Python_followers_25062015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "youtrack_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "WebStormIDE_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "appcode_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rubymine_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "pycharm_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "IntellijIDEA_Followers_10112014"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Jetbrains_team_visitors"
          }, {
            "targetingType" : "List",
            "targetingValue" : "WebStorm_Followers_10112014"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains employees"
          }, {
            "targetingType" : "List",
            "targetingValue" : "PhpStorm_Followers_10112014"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Rider_Evaluators_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Kotlinlang_Visitors_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "ReSharper_Followers_26122014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "AppCode_Followers_26122014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "YouTrack_Followers_26122014"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Opted-Out"
          }, {
            "targetingType" : "List",
            "targetingValue" : "WebStorm_Followers_26112014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "appcode_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "0xdbe_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "TeamCity_Followers_21032015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "PyCharm_Followers_26122014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dottrace_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dotPeek_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dotCover_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "clion_ide_followers_29-05-2014"
          }, {
            "targetingType" : "List",
            "targetingValue" : "0xdbe_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "clion-ide_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "upsource-jb_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dottools_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "appcode_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsSales_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrains_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@jetbrains_mps_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "phpstorm_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "kotlin_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "intellijidea_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dottools_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "datagrip_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "clion_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "pycharm_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "phpstorm_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "mps_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "kotlin_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "clion_ide_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dottools_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "youtrack_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "TeamCity_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "phpstorm_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "kotlin_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "0xdbe_followers_23-09-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rubymine_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "pycharm_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "resharper_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "intellijidea_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "youtrack_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "TeamCity_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "WebStormIDE_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "appcode_followers_04-12-2015"
          }, {
            "targetingType" : "List",
            "targetingValue" : "upsource_jb_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrains_Edu_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "teamcity_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsKtor_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "JetBrains_emails_26.02.18"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rubymine_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@kotlin_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "pycharm_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JBToolbox__followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "youtrack_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsRU_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "webstormide_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsStatus_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@dotCover_followers_28.06.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Dottools_Evaluators_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@datagrip_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsRider_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@jetbrains_space_followers_28.06.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "DataGrip_Evaluators_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@youtrack_followers_28.06.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "WebStorm_Visitors_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@intellijrust_followers_28.06.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "GoLand_Evaluators_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JetBrainsCZ_followers_28.06.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "DataGrip_Visitors_upd"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@intellijidea_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "webstormide_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@JBPlatform_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "upsource_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@GoLandIDE_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "teamcity_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@clion_ide_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rubymine_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@dotPeek_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "appcode_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@kotlinconf_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "0xdbe_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@rubymine_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrains_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@jetbrainsjp_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "youtrack_followers_13-1-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@resharper_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrains_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@phpstorm_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "intellijidea_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@dottrace_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "dottools_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@jetbrains_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "clion_ide_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@appcode_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "phpstorm_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@dotmemory_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "kotlin_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@resharper_cpp_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrainsrider_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@upsource_jb_followers_28.06.2020"
          }, {
            "targetingType" : "List",
            "targetingValue" : "jetbrains_mps_followers_29-4-2016"
          }, {
            "targetingType" : "List",
            "targetingValue" : "@jetbrains_hub_followers_28.06.2020"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-01 19:16:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283025834517946368",
            "tweetText" : "#WhereNext: Das Magazin zu #GIS #LocationIntelligence &amp; #Mapping. Jetzt Newsletter sichern (4x im Jahr) ▶️  https://t.co/1z93rUMKQH https://t.co/gTSbDuladP",
            "urls" : [ "https://t.co/1z93rUMKQH" ],
            "mediaUrls" : [ "https://t.co/gTSbDuladP" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Esri Deutschland",
            "screenName" : "@Esri_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:16:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285875397528367104",
            "tweetText" : "In den 🆕 Video-Tutorials in der @Reporterfabrik räumt Journalistin Maja Weber mit fünf großen Corona-Mythen auf, und du erfährst, wie du Falschmeldungen zum Coronavirus erkennen kannst.\n➡️ https://t.co/zWjhziy16F https://t.co/mN1wEvGzAr",
            "urls" : [ "https://t.co/zWjhziy16F" ],
            "mediaUrls" : [ "https://t.co/mN1wEvGzAr" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CORRECTIV Faktencheck",
            "screenName" : "@correctiv_fakt"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ZDDK_"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@faznet"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SZ"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1161912920596537344",
            "tweetText" : "Looking for a new agency? Preselect the best agencies by shortlisting them using Ad World Masters' shortlist feature.\nRegister today as a Marketer, and find the best agencies for your needs. It's Free! 😉🆓👉https://t.co/89CAObNrmR #agency #marketing https://t.co/KxFQs3oGtK",
            "urls" : [ "https://t.co/89CAObNrmR" ],
            "mediaUrls" : [ "https://t.co/KxFQs3oGtK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Ad World Masters",
            "screenName" : "@adworldmasters"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googleanalytics"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Marketing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "social media"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "contentmarketing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#contentmarketing"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:11:41"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1167626743588503552",
            "tweetText" : "While enjoying any of our work, you are helping our next educational TV series. Thank You!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Jean Mercier presents",
            "screenName" : "@apoemaday"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Comedy"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Comedy"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "teaching"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "education"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "13 and up"
          } ],
          "impressionTime" : "2020-08-01 19:12:13"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287919300657868800",
            "tweetText" : "Fix cloud misconfigurations for AWS, GCP, Terraform, K8s, and CloudFormation in run-time and build-time automatically.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "bridgecrew",
            "screenName" : "@bridgecrewio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@troyhunt"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:42:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240509624559828993",
            "tweetText" : "Collaborate like you’re sitting side-by-side on an online whiteboard. Free forever. No credit card required. https://t.co/O3CfanZE1z https://t.co/ycXjEEwcrg",
            "urls" : [ "https://t.co/O3CfanZE1z" ],
            "mediaUrls" : [ "https://t.co/ycXjEEwcrg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Office365"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Office"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business software"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:40:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "804324559851819008",
            "tweetText" : "Schooling is not virtuous\nIgnorance is not strength\nIndoctrination is not education\n📒: https://t.co/y3gqkMyYpN https://t.co/rAxnxRSKMg",
            "urls" : [ "https://t.co/y3gqkMyYpN" ],
            "mediaUrls" : [ "https://t.co/rAxnxRSKMg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Joss Sheldon 3.5%",
            "screenName" : "@JossSheldon"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Education news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Books news and general info"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amazon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@cnnbrk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TIME"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amnestyusa"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amnesty"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEllenShow"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@guardian"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Oprah"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNICEF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNHumanRights"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Reuters"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NewYorker"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HuffPost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@washingtonpost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hrw"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:11:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283031834281021441",
            "tweetText" : "#KI: Das kann die Technologie bereits - Beispiel #Objekterkennung in Luftbildern: \nhttps://t.co/PTnPzNSkjJ\n#GeoAI #GIS #MachineLearning",
            "urls" : [ "https://t.co/PTnPzNSkjJ" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Esri Deutschland",
            "screenName" : "@Esri_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:42:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "German Doctors e.V.",
            "screenName" : "@germandoctors"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNICEFSchweizFL"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MSF_Schweiz"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MSF_austria"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:27:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286608038544191493",
            "tweetText" : "Lade die frankly App herunter und eröffne einfach und ohne Bankbesuch deine Säule 3a.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "BBC News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Entertainment news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "World news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Sports news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:16:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243538916218724354",
            "tweetText" : "Wie können Sie langfristige Beziehungen zu Ihren Kunden aufbauen? \nLaden Sie unseren Onboarding-Leitfaden für MSPs kostenlos herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer reviews"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "it service"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-01 19:41:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283784015409971205",
            "tweetText" : "CHF 20.–/Mt. Rabatt auf Swiss mobile flat.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@derspiegel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesschau"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NetflixDE"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ZDF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SZ"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "smartphones"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "smartphone"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:29:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1287635557045198848",
            "tweetText" : "Create, collaborate, and centralize communication across your company on a single online whiteboard. Try Miro. https://t.co/LRhKhlbuvs https://t.co/4pcN9uE57N",
            "urls" : [ "https://t.co/LRhKhlbuvs" ],
            "mediaUrls" : [ "https://t.co/4pcN9uE57N" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Pinterest"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:14:56"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194327858216961",
            "tweetText" : "Jetzt lieber durchstarten statt viel Zeit für Bank- und Versicherungstermine aufwenden?",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1029048830006767616",
            "tweetText" : "Here's how to avoid throngs of tourists at the world's most visited attractions.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Far & Wide",
            "screenName" : "@FarandWidecom"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:12:01"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289214243816972289",
            "tweetText" : "Warum bei einer Sanierung drei Dimensionen berücksichtigt werden müssen und was das mit dem CO2-Gesetz zu tun hat: #powernewz #2000wattziele",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "ewz - mehr als Strom",
            "screenName" : "@ewz_energie"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Green solutions"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Construction"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1283023474546610183",
            "tweetText" : "#WhereNext: Inspirationen zu #GIS, #LocationIntelligence &amp; #Mapping - 4x im Jahr ins Postfach. Jetzt sichern ▶️ https://t.co/puSVqDAFKg https://t.co/C22YFuXMbI",
            "urls" : [ "https://t.co/puSVqDAFKg" ],
            "mediaUrls" : [ "https://t.co/C22YFuXMbI" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Esri Deutschland",
            "screenName" : "@Esri_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:29:49"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285194331167629313",
            "tweetText" : "Start a company despite difficult times? It’s easy with our free starter kit.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "boss"
          }, {
            "targetingType" : "List",
            "targetingValue" : "[Adobe DMP Audience] BR-100565 Start Business Inclusion"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289146053288239104",
            "tweetText" : "Der #HarryPotter Charakter wird heute 40 Jahre alt. #feelingoldyet? ;) Falls du die nächsten 20 Stunden noch nichts vor hast, auf Swisscom TV gibt’s alle Filme: https://t.co/Y3U6xxIyOZ https://t.co/lr82lIJA7J",
            "urls" : [ "https://t.co/Y3U6xxIyOZ" ],
            "mediaUrls" : [ "https://t.co/lr82lIJA7J" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Sci-fi and fantasy"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Sci-fi and fantasy"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jk_rowling"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EmmaWatson"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:16:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286607550868344832",
            "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Sports news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "World news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Entertainment news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "BBC News"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:30:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1152315001371336707",
            "tweetText" : "Meet CrossBox - a brand new Webmail App for cPanel, Plesk, and Direct Admin. It's intuitive, it's fast, and it looks pretty darn good! Choose to do email properly, choose the CrossBox #Webmail #App. https://t.co/5Bxtqyjnk0 https://t.co/TKwxSMEEbM",
            "urls" : [ "https://t.co/5Bxtqyjnk0" ],
            "mediaUrls" : [ "https://t.co/TKwxSMEEbM" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CrossBox",
            "screenName" : "@CrossBoxIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:30:31"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288941054226386944",
            "tweetText" : "Why is B2B customer success hard and how do you take the first step? Book your seat today to hear Karl Mosgofian, CIO of @GainsightHQ, and Sameer Dixit of Persistent on how to drive B2B customer success with data stack modernization in our upcoming webinar.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Persistent Systems",
            "screenName" : "@Persistentsys"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#datascience"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:03"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288850390088404993",
            "tweetText" : "💰CHF 6'000💰 CS:GO Tournament! Ready for the CS:GO Cup in @RedBullCH Gaming World by @LogitechG Online Series? 💥\n\n💵 CHF 6'000.-\n📅 05.08.2020 | Qualifier 1\n✏️https://t.co/n4slgo4ENv\n📅 09.08.2020 | Qualifier 2\n✏️https://t.co/RNX5bseUdm\n\nRegister now!\n\n#CSGO https://t.co/ZvZl42aVTj",
            "urls" : [ "https://t.co/n4slgo4ENv", "https://t.co/RNX5bseUdm" ],
            "mediaUrls" : [ "https://t.co/ZvZl42aVTj" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "mYinsanity",
            "screenName" : "@mYinsanityCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Counter-Strike"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1278319989582434306",
            "tweetText" : "Biomarkers in Neuropsychiatry Most Read Article Collection and Call for Papers https://t.co/HioKMScUzG https://t.co/3hubRjDqTx",
            "urls" : [ "https://t.co/HioKMScUzG" ],
            "mediaUrls" : [ "https://t.co/3hubRjDqTx" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Elsevier Psychiatry",
            "screenName" : "@els_psychiatry"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@APAPsychiatric"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:29:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1271392207006846979",
            "tweetText" : "🅴🅻🅰🆂🆃🅸🅲 🆂🆃🅰🅲🅺 Training | 15.-17. September 2020 | München\n🧑‍🎓 Dein praktischer Einstieg ins Suchen, Analysieren und Visualisieren von Daten in Echtzeit. \n\nLimitierte Teilnehmerzahl. Gleich klicken und anmelden!\n👉 https://t.co/FcRvctw8xJ https://t.co/WFh56vMo5a",
            "urls" : [ "https://t.co/FcRvctw8xJ" ],
            "mediaUrls" : [ "https://t.co/WFh56vMo5a" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "NETWAYS Events",
            "screenName" : "@NetwaysEvents"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:35"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276472659656585216",
            "tweetText" : "Part 8 is out! Gotta say this was intense! #TheLastofUsPartII #TheLastofUsPart2 #PlayStation #PS4 #PS4share #Gaming\n\nhttps://t.co/iWuJTSSlII",
            "urls" : [ "https://t.co/iWuJTSSlII" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CoalaTV 🎮",
            "screenName" : "@coalabr14"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Ubisoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RockstarGames"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@bethesda"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@PlayStation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EA_DICE"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DOOM"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Red Dead Redemption"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "PlayStation"
          } ],
          "impressionTime" : "2020-08-01 19:11:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288141250445672456",
            "tweetText" : "Bei der Endfertigung des iPhone entsteht kein Deponiemüll.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Apple",
            "screenName" : "@Apple"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "tierschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "naturschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "land"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tiere"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:41:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288158727615520774",
            "tweetText" : "Verfolge, jage und studiere die Wunder des Tierreiches als Naturkundler, der neuen Tätigkeit im Grenzland in Red Dead Online.\n\nErforsche verschiedene Gebiete, spüre legendäre Tiere auf, setze neue Waffen ein und mehr.\n\nJetzt spielen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Rockstar Games",
            "screenName" : "@RockstarGames"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Ubisoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Steam"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Xbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Halo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@bethesda"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@PlayStation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GameSpot"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IGN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BethesdaStudios"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EA"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#nowplaying"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "modded"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#androidgames"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#girlgamer"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#modding"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#glitches"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "modding"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "backward compatible"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-latam-asia-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-001-rdops4xb1-activeplayers-latam-asia-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-001-rdops4xb1-activeplayers-eu-18ce54s7sud"
          }, {
            "targetingType" : "List",
            "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-eu-18ce54s7sud"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276536962556583939",
            "tweetText" : "“It's become a strategic game of chess rather than monopoly, it's not about sitting on Park Lane and collecting $200.” Ashton Reid talks to Kim Catechis about property in our #AFTERMATH series.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Martin Currie",
            "screenName" : "@MartinCurrieIM"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:11:35"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286911813695807489",
            "tweetText" : "Wir können uns gegenseitig schützen und gleichzeitig wieder eine Stimmung des Vertrauens in der Gesellschaft schaffen: Die Lösung wäre einfach.\nhttps://t.co/rkq3DmEv8p",
            "urls" : [ "https://t.co/rkq3DmEv8p" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "RefLab",
            "screenName" : "@ref_lab"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kathch"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:34:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275062714025431041",
            "tweetText" : "Auch die Aufnahme des 3. #EduDays ist nun online verfügbar. Sehen Sie sich gerne (nochmal) die Inputs samt Präsentationen von @GromovaKatarina, @SchulerAnita, @Beedle_co und @GoyMichael rund um Erklärvideos, Teams und OneNote an. #MicrosoftEdu #PiLSchweiz",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#unterricht"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Unterricht"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1285599885589282823",
            "tweetText" : "Check out 15+ code editors for a range of programming languages and technologies, all available in one app developed by #JetBrains.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "JetBrains",
            "screenName" : "@jetbrains"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ThePSF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledevs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reddit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Microsoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@VisualStudio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#firmware"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#embeddedlinux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#c#embedded"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#embedded"
          }, {
            "targetingType" : "List",
            "targetingValue" : "JetBrains - Twitter followers - 25.02.2020"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Opted-Out"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains_Buyers_upd"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "ToolBox_Evaluators"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "Jetbrains_team_visitors"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "JetBrains employees"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:30:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1289185137935220737",
            "tweetText" : "Der Grundsatz der #Materialitaet ist gerade in der hoch emotional geführten #Nachhaltigkeitsdebatte ein zentraler Aspekt. Wie lässt sich die #Emotion aus der Bewertung herausnehmen, um eine objektive #Risikobewertung zu erhalten? https://t.co/jLZpEP5lmK",
            "urls" : [ "https://t.co/jLZpEP5lmK" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CAPinside",
            "screenName" : "@CAPinside"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Investieren"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282662305923248129",
            "tweetText" : "🐎 Der Traum vom eigenen Pferd: Wie Ihr ihn Euch erfüllt und welche Kosten auf Euch zukommen:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:29:46"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Helvetia Schweiz",
            "screenName" : "@helvetia"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:39:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275026152055361536",
            "tweetText" : "Pack your swimming trunks and grab your new iPhone 11. Now with a CHF 100 discount!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BillGates"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@elonmusk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SpaceX"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitter"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleSupport"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JeffBezos"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sundarpichai"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppStore"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AppleMusic"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@iphone_dev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mashable"
          }, {
            "targetingType" : "Retargeting user engager",
            "targetingValue" : "Retargeting user engager: 98382537"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iOS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:40:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288051575504740354",
            "tweetText" : "Durch Homeschooling in Coronazeiten musste der Schulunterricht in kürzester Zeit digitalisiert werden. Viele Eltern haben so gemerkt, dass der Lerneffekt ihrer Kinder durch Smartphones und Tablets sogar steigen kann. Hat das herkömmliche Klassenzimmer ausgedient? https://t.co/U2wjIc1dtU",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/U2wjIc1dtU" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Michael In Albon, Medienkompetenzexperte",
            "screenName" : "@MichaelInAlbon"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "eltern"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "kids"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "mutter"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "education"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "vater"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "eduction"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "teaching"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "educations"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:39:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288136672878768129",
            "tweetText" : "#Öl\nGewinn: $1,503\nPreis 27. Juli 2020:$41.74\nPreis am 20. April 2020:$16.67\n⤴️150.39%\n\n#Silber\nGewinn:$1,054\nPreis am 27. Juli 2020:$25.59\nPreis am 16. März 2020:$12.46\n⤴️105.42%%\n\n#GOLD \nGewinn:$853\nPreis am 27. Juli 2020:$1957.8\nPreis am 05. Juli 2019:$1496.6\n⤴️85.36% https://t.co/sAYoqw8WnO",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/sAYoqw8WnO" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "eToro_DACH",
            "screenName" : "@eToro_DACH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Science news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Computer gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "bitcoin"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "anlegen"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "investieren"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:42:21"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288689645690261506",
            "tweetText" : "Gold demand falls 11% in Q2 2020 as gold prices and demand for gold-backed ETFs both reach record highs. Explore Q2 and H1 2020 gold market insights in the latest issue of Gold Demand Trends.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "World Gold Council",
            "screenName" : "@GOLDCOUNCIL"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Reuters"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-01 19:42:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1284053244671664129",
            "tweetText" : "Get your craft tools out! ✂️📜🖍️\n\nIn collaboration with @NintendoDE, we're giving away 2 copies of the new #PaperMarioTheOrigamiKing.\n\nTo win, you have to post your best Nintendo Papercraft, Drawing, Origami, etc. as a reply to this tweet. \n\nYou have until July 31st, good luck! https://t.co/d3KMCBU0kW",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/d3KMCBU0kW" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom Gaming",
            "screenName" : "@Swisscom_Gaming"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Nintendo"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Nintendo"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Video Games"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 19:41:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Basler Kantonalbank",
            "screenName" : "@BaslerKB"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-01 20:19:12"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244996725967851522",
            "tweetText" : "It's our most-requested feature ever... and now the Prefect UI is open-source! 🎉\n￼\nRun `prefect server start` to get going - it's really that easy.\n\nhttps://t.co/oNebWlZNNu",
            "urls" : [ "https://t.co/oNebWlZNNu" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Prefect",
            "screenName" : "@PrefectIO"
          },
          "impressionTime" : "2020-08-03 22:09:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286695569315028992",
            "tweetText" : "Visit Greece, where summer is a state of mind.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bloomberg",
            "screenName" : "@business"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "business and finance"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-03 22:21:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "VMware"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "IT security"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-03 22:00:20"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290208657267982336",
            "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-05 01:32:10"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "HCL Software",
            "screenName" : "@HCLSoftware"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business software"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-05 01:32:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "VMware"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#virtualization"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "virtualization"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-05 01:34:33"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1291253785965322243",
            "tweetText" : "Für unsere Kunden ist eine Immobilie das grösste Investment ihres Lebens. Deshalb bieten wir einen einmaligen, digitalen Service – inklusive persönlicher Beratung. #hausfinanzierung #hypothek #immobilienberatung https://t.co/bS12ScnIfV",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/bS12ScnIfV" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "key4 by UBS",
            "screenName" : "@key4byUBS"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Sparen"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 18:48:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-06 18:48:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1204325243432656896",
            "tweetText" : "Create, collaborate, and centralize communication for all your\ncross-functional team work. Try Miro! https://t.co/iP3MTOfZbP https://t.co/gevlg0g46g",
            "urls" : [ "https://t.co/iP3MTOfZbP" ],
            "mediaUrls" : [ "https://t.co/gevlg0g46g" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SamsungMobileUS"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IBM"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-06 23:33:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1267812108739067907",
            "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bank CIC",
            "screenName" : "@Bank_CIC"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#investments"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "investments"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#investment"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 23:38:40"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1274039556514492417",
            "tweetText" : ".@EnvyJayne brings esports pros @Wizzrobe, @At0micRL, and @notevenbleu together to give their thoughts on clips submitted by fans. \n\nSend us your clips: https://t.co/15tQnBwdv1 https://t.co/w1PvQmt32m",
            "urls" : [ "https://t.co/15tQnBwdv1" ],
            "mediaUrls" : [ "https://t.co/w1PvQmt32m" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Envy",
            "screenName" : "@Envy"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Call of Duty"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Overwatch"
          } ],
          "impressionTime" : "2020-08-06 23:45:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1282979307615387648",
            "tweetText" : "Was macht den griechischen Sommer so besonders? Es ist nicht das Meer. Es ist nicht die Sonne. Es ist mehr als das. Lass es uns herausfinden ... #visitgreece #GreekSummerFeeling #EndlessGreeekSummer https://t.co/P0T43a99GH",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/P0T43a99GH" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Visit Greece",
            "screenName" : "@VisitGreecegr"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@travel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NatGeoTravel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TravelLeisure"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-06 23:54:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-06 23:33:56"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-06 23:45:46"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-06 20:14:07"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1190243947156774913",
            "tweetText" : "Welcome to Kialo Edu, the tool used by educators world-wide to teach critical thinking and facilitate thoughtful classroom debate. Kialo Edu is a free version of Kialo, the world's most popular argument mapping site, customized for classroom use. Try it! https://t.co/c2U5ZEYcz7",
            "urls" : [ "https://t.co/c2U5ZEYcz7" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Kialo Edu",
            "screenName" : "@KialoEdu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#edtech"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 20:14:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290292611039846401",
            "tweetText" : "Lerne die Vielfalt von Teams mit Hilfe unserer Experten in kostenlosen und digitalen Sitzungen kennen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Schweiz",
            "screenName" : "@microsoft_ch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "@microsoft"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 20:26:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290615454961152000",
            "tweetText" : "Welches ist das perfekte Gaming Smartphone? 🤔\nZum Start der neuen #SwisscomHeroLeague Saison haben wir eine Checkliste für das perfekte mobile Spielerlebnis zusammengestellt: https://t.co/ydyLJG0uT0 https://t.co/bdqMuuvt65",
            "urls" : [ "https://t.co/ydyLJG0uT0" ],
            "mediaUrls" : [ "https://t.co/bdqMuuvt65" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Online gaming"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@LeagueOfLegends"
          }, {
            "targetingType" : "Followers of a user id",
            "targetingValue" : "@Swisscom_de"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gaming"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 20:18:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "PlayStationDE",
            "screenName" : "@PlayStationDE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "PlayStation"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Nintendo"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "PC gaming"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Xbox"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "games"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 19:18:14"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290208657267982336",
            "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-06 19:18:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1291413625907814403",
            "tweetText" : "Join us next week on August 13th for our next TezTalks LIVE stream featuring Stephane De Baets of Elevated Returns. \n\nWe will also be giving away another #Tezos branded @ledger Nano S during the live stream. \n\nTune in for your chance to win!\n\nhttps://t.co/0JLvJwwrvP",
            "urls" : [ "https://t.co/0JLvJwwrvP" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Tezos Commons",
            "screenName" : "@TezosCommons"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-07 00:00:15"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileAccountsSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Huawei",
            "screenName" : "@Huawei"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nokia"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Facebook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Tesla"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Microsoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@YouTube"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Windows"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IBM"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@intel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Sony"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#technology"
          }, {
            "targetingType" : "List",
            "targetingValue" : "2020 Exclude List V2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:14:35"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290208657267982336",
            "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:21:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "SearchTweets",
          "promotedTweetInfo" : {
            "tweetId" : "1290208657267982336",
            "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:14:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "HCL Software",
            "screenName" : "@HCLSoftware"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Business Technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:14:35"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "HCL Software",
            "screenName" : "@HCLSoftware"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Business Technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:14:11"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileTweets",
          "promotedTweetInfo" : {
            "tweetId" : "1290208657267982336",
            "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 10:14:35"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286695569315028992",
            "tweetText" : "Visit Greece, where summer is a state of mind.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bloomberg",
            "screenName" : "@business"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "business and finance"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-09 19:19:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290301401646665735",
            "tweetText" : "Shift Code Quality and Security left to your #pullRequest! SonarCloud detects the issues in your PR and development branches. You also get clear remediation guidance on fixing them. So your code instantly gets cleaner and safer. https://t.co/skTX0ZMvNc #codequality #codesecurity",
            "urls" : [ "https://t.co/skTX0ZMvNc" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "SonarCloud",
            "screenName" : "@SonarCloud"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-09 19:50:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "VMware"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Networking"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "server"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "IT security"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:50:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "VMware"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Networking"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "IT security"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "server"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:23:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lohika",
            "screenName" : "@Lohika"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@cnnbrk"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@cnni"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BreakingNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CBSNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ABC"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNNPolitics"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          } ],
          "impressionTime" : "2020-08-09 19:19:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1291667961208594434",
            "tweetText" : "Das ist Paula – Swisscom Kundin\nStudentin, Leseratte und #bereit fürs Leben. https://t.co/E5Vv7SfxMT",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/E5Vv7SfxMT" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Retargeting campaign engager",
            "targetingValue" : "Retargeting campaign engager: 24040764"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:25:39"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1250881376435318784",
            "tweetText" : "Bots account for 50% of all web traffic. Today, more than ever, #DevSecOps experts need improved defenses against bot-generated traffic.\n\nThis ebook reveals the ways attackers use #bots and how to defend against the most common attacks.\n\nGet your copy 👇",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:28:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1258418906399735814",
            "tweetText" : "Researchers studied the effects of playing #videogames on the #cognitive capacity to distinguish between important and irrelevant information within a rapid stream of #VisualStimuli. #Neuroscience\n\nRead #openaccess study published in @FrontNeurosci https://t.co/c1r2LGTeIl https://t.co/S30WTNeWZO",
            "urls" : [ "https://t.co/c1r2LGTeIl" ],
            "mediaUrls" : [ "https://t.co/S30WTNeWZO" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Frontiers",
            "screenName" : "@FrontiersIn"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HumanBrainProj"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#deepbrainstimulation"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          } ],
          "impressionTime" : "2020-08-09 19:20:14"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1286607550868344832",
            "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCWorld"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CNN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BBCBreaking"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "World news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "BBC News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Sports news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Entertainment news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business news"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#investment"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "investments"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "financing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "finance"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:29:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Google Cloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "VMware"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "IT security"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Networking"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "server"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:25:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290302195905306624",
            "tweetText" : "Tourism’s collapse could trigger next stage of the crisis https://t.co/hUBz1dWac3",
            "urls" : [ "https://t.co/hUBz1dWac3" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Financial Times",
            "screenName" : "@FinancialTimes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EUCouncil"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nytimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EU_Commission"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheEconomist"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NYSE"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@washingtonpost"
          }, {
            "targetingType" : "List"
          }, {
            "targetingType" : "List"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-09 19:27:00"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1276138251322081281",
            "tweetText" : "Join leading academics and tech professionals from around the world to examine critical issues around AI for privacy and security at CyberSec&amp;AI Connected. JUNE 30 is your last chance for an early bird discount. Book now ➤ https://t.co/7ecFNlTenD https://t.co/jNEqup0RSM",
            "urls" : [ "https://t.co/7ecFNlTenD" ],
            "mediaUrls" : [ "https://t.co/jNEqup0RSM" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CyberSec&AI Connected",
            "screenName" : "@CyberSecAI"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Events",
            "targetingValue" : "Cyber Week 2020"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:44:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Milchproduzenten SMP / Producteurs de Lait PSL",
            "screenName" : "@SMP_PSL"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "switzerland"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:40:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Milchproduzenten SMP / Producteurs de Lait PSL",
            "screenName" : "@SMP_PSL"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "switzerland"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:37:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1270656956504641537",
            "tweetText" : "#Barrierefrei posten: In den Barrierefreiheit-Einstellungen \"Bildbeschreibungen verfassen\" aktivieren. Beim Posten eines Fotos unten rechts auf den +ALT-Button tippen und Text eingeben.\n#gemeinsambereit #bereit https://t.co/90SAooVT4y",
            "urls" : [ ],
            "mediaUrls" : [ "https://t.co/90SAooVT4y" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amnesty"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:37:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1275813719935717377",
            "tweetText" : "Sie haben einen Covid-19 Kredit beansprucht? Dann brauchen Sie eine sorgfältig geplante Exit-Strategie und sollten sich bereits über die Zeit nach der Pandemie Gedanken machen. UBS steht Ihnen dabei mit Ratschlägen zur Seite: https://t.co/DHw2lSNF39 https://t.co/AdR6mKjHeE",
            "urls" : [ "https://t.co/DHw2lSNF39" ],
            "mediaUrls" : [ "https://t.co/AdR6mKjHeE" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Entrepreneurship"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Leadership"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-06-26 21:40:57"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-17 18:34:40"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Bloomberg QuickTake",
            "screenName" : "@QuickTake"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WFP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@fema"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNESCO"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedCross"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Refugees"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@amnestyusa"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ICRC"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CARE"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ONECampaign"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@USAID"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNICEF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@unfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MSF_USA"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNDP"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WHO"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SavetheChildren"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gatesfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Change"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hrw"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Non-profit"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#givingback"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#kindness"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "kindness"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#backing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#humanity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "employee retention"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#employeeengagement"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "backing"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "humanity"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-18 23:06:22"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Janssen CH",
            "screenName" : "@Janssen_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@seeklinik"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Psychiatrie"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-21 23:58:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1293125812326215680",
            "tweetText" : "«Auf dem Land können sich nicht nur meine Kinder austoben», Swisscom Kunde Valentin lebt als Freischaffender auf dem Land. #bereit",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "promotedTrendInfo" : {
            "trendId" : "74256",
            "name" : "#bereit",
            "description" : "Das gute Gefühl bereit zu sein. Bereit fürs Leben."
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-21 23:58:06"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Janssen CH",
            "screenName" : "@Janssen_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@seeklinik"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-23 00:29:21"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Janssen CH",
            "screenName" : "@Janssen_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@seeklinik"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-23 00:19:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1291376350150955008",
            "tweetText" : "Become President and defend your country in World War III! Register and play for free, no download required.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Conflict Of Nations",
            "screenName" : "@CoNWW3"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Commentary"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Poland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-08-23 00:19:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1296504955574956032",
            "tweetText" : "Your family corner in VR is now available for free on #OculusQuest. From games to adventure to entertainment, Alcove is packed with activities for the whole family. Download Alcove today and start connecting with your family in VR!\nhttps://t.co/Lha7aCXHPp https://t.co/ZnsSGSHiKc",
            "urls" : [ "https://t.co/Lha7aCXHPp" ],
            "mediaUrls" : [ "https://t.co/ZnsSGSHiKc" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Alcove VR",
            "screenName" : "@AlcoveVR"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sibleyspeaks"
          } ],
          "impressionTime" : "2020-08-23 00:29:28"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "IP Progress",
            "screenName" : "@IPProgress"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@StateDept"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WHO"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Pharma"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "pharmaceutical"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "health"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-23 00:19:11"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1293643657409634312",
            "tweetText" : "Collect every single log from your app while only paying for what you analyze. Datadog’s Logging Without Limits™ allows you to ingest all logs while keeping your costs under control.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#debugging"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#SIEM"
          }, {
            "targetingType" : "List",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Poland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-23 00:29:28"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1288519058845908992",
            "tweetText" : "We built FaunaDB so you can focus on building your app instead of data infrastructure. See what others are saying!\n\nTry FaunaDB: https://t.co/48mbnD2Up2 https://t.co/v7gUcMMR4c",
            "urls" : [ "https://t.co/48mbnD2Up2" ],
            "mediaUrls" : [ "https://t.co/v7gUcMMR4c" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "JAMstack"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "netlify"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-24 16:41:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Janssen CH",
            "screenName" : "@Janssen_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@seeklinik"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Psychiatrie"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-24 16:40:03"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Janssen CH",
            "screenName" : "@Janssen_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Psychiatrie"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-25 20:09:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1290640895361130506",
            "tweetText" : "Mit Google Ads sind Sie präsent, wenn Kunden online suchen. Legen Sie jetzt gleich los.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Google Ads",
            "screenName" : "@GoogleAds"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Inc"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hootsuite"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kickstarter"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Germany"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-08-25 20:09:47"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "IP Progress",
            "screenName" : "@IPProgress"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Politics"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Health news and general info"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@StateDept"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UN"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WHO"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WorldBank"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Political News"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Pharma"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-08-26 22:44:53"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SecurityWeek"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@threatpost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-06-30 23:47:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1268255650313719808",
            "tweetText" : "How powerful is your application security? ⚡️Take the grader and get actionable steps to improve the security of your web apps. ✅",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "mongodb"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "redis"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "cloudflare"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-06-30 23:47:55"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SecurityWeek"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@threatpost"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@e_kaspersky"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-01 10:39:46"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "BBB Studios",
            "screenName" : "@BBB_Studios"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Twitch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Treyarch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@aplusk"
          } ],
          "impressionTime" : "2020-07-01 21:00:00"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240509624559828993",
            "tweetText" : "Collaborate like you’re sitting side-by-side on an online whiteboard. Free forever. No credit card required. https://t.co/O3CfanZE1z https://t.co/ycXjEEwcrg",
            "urls" : [ "https://t.co/O3CfanZE1z" ],
            "mediaUrls" : [ "https://t.co/ycXjEEwcrg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Graphics software"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Website Activity",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-05 01:31:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Beyond Identity",
            "screenName" : "@beyondidentity"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-05 01:31:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245709288162709505",
            "tweetText" : "If your #microservice architecture is not delivering on the promise of increased agility, scalability &amp; resiliency, download this guide: https://t.co/dvPoqcsMJw https://t.co/8eX6bPmYuG",
            "urls" : [ "https://t.co/dvPoqcsMJw" ],
            "mediaUrls" : [ "https://t.co/8eX6bPmYuG" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Solace",
            "screenName" : "@solacedotcom"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "enterprise architects"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-05 01:37:31"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243538916218724354",
            "tweetText" : "Wie können Sie langfristige Beziehungen zu Ihren Kunden aufbauen? \nLaden Sie unseren Onboarding-Leitfaden für MSPs kostenlos herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer reviews"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "it service"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-06 01:48:19"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Citrix",
            "screenName" : "@citrix"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#ServiceNow"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "ServiceNow"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "@servicenow"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-06 01:48:18"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-06 01:50:50"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-09 23:59:46"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "WhiteSource",
            "screenName" : "@WhiteSourceSoft"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-09 23:57:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1280858925491990528",
            "tweetText" : "Zwischen remote arbeiten und sicher arbeiten – Webex. Erfahren Sie mehr über unsere sichere, zuverlässige Videokommunikation: https://t.co/Ab0EPM8mvB",
            "urls" : [ "https://t.co/Ab0EPM8mvB" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Cisco Switzerland",
            "screenName" : "@Cisco_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Education news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MensHealthMag"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Skype"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WomensHealthMag"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Artificial intelligence"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Education Related"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-09 23:57:49"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1262853920055013379",
            "tweetText" : "Crypto Craft provides high-quality information to traders.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Crypto Craft",
            "screenName" : "@CryptoCraft"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-07-10 00:00:20"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Glencore",
            "screenName" : "@Glencore"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-07-27 15:36:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "861653651630288896",
            "tweetText" : "Yeah! Here's how you can actually travel for less...  https://t.co/BJBbMpesaZ",
            "urls" : [ "https://t.co/BJBbMpesaZ" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bella",
            "screenName" : "@Three3Steps"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "National parks"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-07-27 15:36:14"
        } ]
      }
    }
  }
} ]