window.YTD.ad_engagements.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1275135887354089474",
              "tweetText" : "Chokes are EMC components that share aspects with inductors and transformers. Join our June 24 #webinar at 9 A.M. EDT/3 P.M. CEST to explore the construction and characteristics of different types of chokes: https://t.co/KLltIkHKrJ #KEMETishere for #engineers #engineering #tech https://t.co/ghSYh8hZ5w",
              "urls" : [ "https://t.co/KLltIkHKrJ" ],
              "mediaUrls" : [ "https://t.co/ghSYh8hZ5w" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "KEMET Electronics",
              "screenName" : "@KEMETCapacitors"
            },
            "impressionTime" : "2020-06-24 01:01:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-06-24 01:01:26",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-06-24 01:01:03",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-06-24 01:01:09",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-06-24 01:01:24",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-06-24 01:02:12",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-06-24 01:01:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-06-24 01:01:14",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-06-24 01:01:20",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-06-24 01:01:05",
            "engagementType" : "VideoContentMrcView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1270656956504641537",
              "tweetText" : "#Barrierefrei posten: In den Barrierefreiheit-Einstellungen \"Bildbeschreibungen verfassen\" aktivieren. Beim Posten eines Fotos unten rechts auf den +ALT-Button tippen und Text eingeben.\n#gemeinsambereit #bereit https://t.co/90SAooVT4y",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/90SAooVT4y" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@amnesty"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-06-26 21:37:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-06-26 21:44:00",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-06-26 21:38:16",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-06-26 21:37:56",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-06-26 21:38:03",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-06-26 21:37:53",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-06-26 21:38:00",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-06-26 21:38:09",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-06-26 21:38:12",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-06-26 21:38:05",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-06-26 21:37:54",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1290208657267982336",
              "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-09 10:14:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-09 10:21:42",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-09 10:21:40",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1290208657267982336",
              "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-09 10:14:44"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-09 10:15:37",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-08-09 10:15:11",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-09 10:14:58",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-09 10:15:24",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-09 10:14:48",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-09 10:21:38",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-09 10:15:35",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-08-09 10:14:46",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-09 10:14:52",
            "engagementType" : "VideoContent6secView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1291667961208594434",
              "tweetText" : "Das ist Paula – Swisscom Kundin\nStudentin, Leseratte und #bereit fürs Leben. https://t.co/E5Vv7SfxMT",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/E5Vv7SfxMT" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Retargeting campaign engager",
              "targetingValue" : "Retargeting campaign engager: 24040764"
            }, {
              "targetingType" : "Retargeting engagement type",
              "targetingValue" : "Retargeting engagement type: 2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-09 19:25:37"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-09 19:25:46",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-08-09 19:26:09",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-08-09 19:25:47",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-09 19:26:41",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-09 19:26:11",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-08-09 19:25:42",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-09 19:26:03",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-09 19:25:55",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-09 19:25:40",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1286607550868344832",
              "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCNews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nytimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCWorld"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CNN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCBreaking"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "BBC News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Political News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Sports news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entertainment news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Business news"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#investment"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "financing"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "finance"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-09 19:29:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-09 19:29:03",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-09 19:29:07",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-09 19:29:05",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-09 19:29:08",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-09 19:29:05",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-09 19:29:03",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1293643657409634312",
              "tweetText" : "Collect every single log from your app while only paying for what you analyze. Datadog’s Logging Without Limits™ allows you to ingest all logs while keeping your costs under control.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#debugging"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#SIEM"
            }, {
              "targetingType" : "List",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Poland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-08-23 00:29:26"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-23 00:29:32",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-23 00:29:31",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-08-23 00:29:33",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-08-23 00:29:33",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-08-23 00:29:36",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-08-23 00:29:30",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-07-06 01:50:49"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-06 01:50:50",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1280858925491990528",
              "tweetText" : "Zwischen remote arbeiten und sicher arbeiten – Webex. Erfahren Sie mehr über unsere sichere, zuverlässige Videokommunikation: https://t.co/Ab0EPM8mvB",
              "urls" : [ "https://t.co/Ab0EPM8mvB" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Cisco Switzerland",
              "screenName" : "@Cisco_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Education news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@MensHealthMag"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Skype"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WomensHealthMag"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Artificial intelligence"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Education Related"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-09 23:57:47"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-09 23:57:49",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-07-09 23:59:45"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-09 23:59:46",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184122352755453952",
              "tweetText" : "Datadog's Logging Without Limits™ allows you to cost-effectively collect and manage all your logs. No longer worry about leaving behind logs that matter - ingest them all while keeping your costs under control. Learn more:  https://t.co/MnoLgq4YuK",
              "urls" : [ "https://t.co/MnoLgq4YuK" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer programming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Open source"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "List",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-07-31 11:16:27"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 11:17:00",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-07-31 11:16:43",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-07-31 11:16:29",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 11:16:58",
            "engagementType" : "VideoContentPlayback50"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1267812108739067907",
              "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bank CIC",
              "screenName" : "@Bank_CIC"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 11:29:44"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 11:29:45",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-07-31 11:29:46",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 11:29:48",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-07-31 11:30:16",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-07-31 11:29:52",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-07-31 11:30:05",
            "engagementType" : "VideoContentPlayback25"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1286607550868344832",
              "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCNews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nytimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCWorld"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CNN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCBreaking"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entertainment news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "BBC News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Political News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Business news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Sports news"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 11:30:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 11:30:58",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-07-31 11:31:00",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-07-31 11:31:01",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-07-31 11:30:57",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-07-31 11:30:54",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 11:30:59",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-07-31 11:30:55",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-07-31 11:30:57",
            "engagementType" : "VideoContentMrcView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288793036881891330",
              "tweetText" : "Smarter Helfer für zuhause – mit der Swisscom Home App lässt sich die WLAN-Verbindung des Routers individuell terminieren. So kann bspw. auch Strom eingespart werden.\nMehr dazu: https://t.co/nlfJSXoHz1 https://t.co/ExxStgkoeK",
              "urls" : [ "https://t.co/nlfJSXoHz1" ],
              "mediaUrls" : [ "https://t.co/ExxStgkoeK" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 11:10:38"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 11:10:41",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288158727615520774",
              "tweetText" : "Verfolge, jage und studiere die Wunder des Tierreiches als Naturkundler, der neuen Tätigkeit im Grenzland in Red Dead Online.\n\nErforsche verschiedene Gebiete, spüre legendäre Tiere auf, setze neue Waffen ein und mehr.\n\nJetzt spielen.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Rockstar Games",
              "screenName" : "@RockstarGames"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ubisoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Steam"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Xbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Halo"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@bethesda"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@PlayStation"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@GameSpot"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IGN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BethesdaStudios"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@EA"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#modding"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamers"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "modding"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "backward compatible"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#streaming"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#nowplaying"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#androidgames"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "modded"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#games"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gaming"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-latam-asia-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-001-rdops4xb1-activeplayers-latam-asia-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-001-rdops4xb1-activeplayers-eu-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-eu-18ce54s7sud"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 15:10:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 16:05:11",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-07-31 16:05:09",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-07-31 16:05:09",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 16:05:12",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288144437630111747",
              "tweetText" : "Apple hat einen Demontage-Roboter für das iPhone entwickelt.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Apple",
              "screenName" : "@Apple"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "naturschutz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tierschutz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "land"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tiere"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-08-01 01:21:19"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 01:21:28",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-08-01 11:39:26",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 01:21:33",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-08-01 01:21:24",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 01:22:35",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:33:24",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 01:21:21",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 01:21:31",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-01 01:21:34",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-08-01 01:21:27",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-01 01:21:24",
            "engagementType" : "VideoContentMrcView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1283375475931451400",
              "tweetText" : "Der #GhostofTsushima erhebt sich auf #PS4 und stellt sich gegen die mongolischen Besatzer.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PlayStationDE",
              "screenName" : "@PlayStationDE"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 22:32:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:34:35",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:34:27",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1284006965912494080",
              "tweetText" : "Niemand kann die Zukunft vorhersagen. Doch mit Derivaten kann man darauf wetten.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Forbes"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 22:32:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:34:14",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:34:19",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1273298030113034240",
              "tweetText" : "Starte dein NETWAYS Managed Kubernetes und beginne in wenigen Minuten mit Deiner Container-Plattform. Setup, Updates, Betrieb übernehmen wir für Dich. Managed Kubernetes ist wie für Dich gemacht. \nJetzt gleich loslegen!\n✅ https://t.co/wt3kwIsj0M https://t.co/5dQOGU31J6",
              "urls" : [ "https://t.co/wt3kwIsj0M" ],
              "mediaUrls" : [ "https://t.co/5dQOGU31J6" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "NETWAYS Web Services",
              "screenName" : "@NetwaysCloud"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@kubernetesio"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 22:32:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:34:45",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-08-01 19:35:01",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:34:46",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1286911813695807489",
              "tweetText" : "Wir können uns gegenseitig schützen und gleichzeitig wieder eine Stimmung des Vertrauens in der Gesellschaft schaffen: Die Lösung wäre einfach.\nhttps://t.co/rkq3DmEv8p",
              "urls" : [ "https://t.co/rkq3DmEv8p" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "RefLab",
              "screenName" : "@ref_lab"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@kathch"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 22:32:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:34:16",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288158727615520774",
              "tweetText" : "Verfolge, jage und studiere die Wunder des Tierreiches als Naturkundler, der neuen Tätigkeit im Grenzland in Red Dead Online.\n\nErforsche verschiedene Gebiete, spüre legendäre Tiere auf, setze neue Waffen ein und mehr.\n\nJetzt spielen.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Rockstar Games",
              "screenName" : "@RockstarGames"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ubisoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Steam"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Xbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Halo"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@bethesda"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@PlayStation"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@GameSpot"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IGN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BethesdaStudios"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@EA"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#nowplaying"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "modded"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#androidgames"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#girlgamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#modding"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#glitches"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "modding"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "backward compatible"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-latam-asia-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-001-rdops4xb1-activeplayers-latam-asia-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-001-rdops4xb1-activeplayers-eu-18ce54s7sud"
            }, {
              "targetingType" : "List",
              "targetingValue" : "rdo-002-rdops4xb1-lapsedplayers-eu-18ce54s7sud"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 20:05:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:42:03",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:42:00",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:41:37",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288051575504740354",
              "tweetText" : "Durch Homeschooling in Coronazeiten musste der Schulunterricht in kürzester Zeit digitalisiert werden. Viele Eltern haben so gemerkt, dass der Lerneffekt ihrer Kinder durch Smartphones und Tablets sogar steigen kann. Hat das herkömmliche Klassenzimmer ausgedient? https://t.co/U2wjIc1dtU",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/U2wjIc1dtU" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Medienkompetenzexperte",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 20:40:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:40:15",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288141250445672456",
              "tweetText" : "Bei der Endfertigung des iPhone entsteht kein Deponiemüll.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Apple",
              "screenName" : "@Apple"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tierschutz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "naturschutz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "land"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tiere"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-07-31 20:22:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:41:05",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:41:08",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288142670444068864",
              "tweetText" : "Apple recycelt dein Smartphone. Auch wenn es kein iPhone ist.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Apple",
              "screenName" : "@Apple"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "land"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tiere"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tierschutz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "naturschutz"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-07-31 20:05:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:42:06",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:42:03",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1194259127419015168",
              "tweetText" : "How does morning classroom lighting impact alertness and mood in students? Read the article here: https://t.co/mBToPX1PKN. https://t.co/UageVL2KgC",
              "urls" : [ "https://t.co/mBToPX1PKN" ],
              "mediaUrls" : [ "https://t.co/UageVL2KgC" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Scientific Reports",
              "screenName" : "@SciReports"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nature"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 20:40:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:40:59",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:40:53",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1271392207006846979",
              "tweetText" : "🅴🅻🅰🆂🆃🅸🅲 🆂🆃🅰🅲🅺 Training | 15.-17. September 2020 | München\n🧑‍🎓 Dein praktischer Einstieg ins Suchen, Analysieren und Visualisieren von Daten in Echtzeit. \n\nLimitierte Teilnehmerzahl. Gleich klicken und anmelden!\n👉 https://t.co/FcRvctw8xJ https://t.co/WFh56vMo5a",
              "urls" : [ "https://t.co/FcRvctw8xJ" ],
              "mediaUrls" : [ "https://t.co/WFh56vMo5a" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "NETWAYS Events",
              "screenName" : "@NetwaysEvents"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 20:22:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:42:02",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:41:36",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:41:35",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288136672878768129",
              "tweetText" : "#Öl\nGewinn: $1,503\nPreis 27. Juli 2020:$41.74\nPreis am 20. April 2020:$16.67\n⤴️150.39%\n\n#Silber\nGewinn:$1,054\nPreis am 27. Juli 2020:$25.59\nPreis am 16. März 2020:$12.46\n⤴️105.42%%\n\n#GOLD \nGewinn:$853\nPreis am 27. Juli 2020:$1957.8\nPreis am 05. Juli 2019:$1496.6\n⤴️85.36% https://t.co/sAYoqw8WnO",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/sAYoqw8WnO" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "eToro_DACH",
              "screenName" : "@eToro_DACH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Science news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "bitcoin"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "anlegen"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investieren"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 20:05:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:42:26",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:42:22",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282660312953618432",
              "tweetText" : "Die Freude über eigene Kinder ist unbezahlbar. 👨‍👩‍👧 Doch was kostet es, wenn man plötzlich zu dritt ist und wie finanziert man das?",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 19:38:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 19:38:17",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1267812108739067907",
              "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bank CIC",
              "screenName" : "@Bank_CIC"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 21:26:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:39:06",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1283375475931451400",
              "tweetText" : "Der #GhostofTsushima erhebt sich auf #PS4 und stellt sich gegen die mongolischen Besatzer.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PlayStationDE",
              "screenName" : "@PlayStationDE"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 18:43:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 18:45:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 19:34:45",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-07-31 18:46:01",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-07-31 18:46:00",
            "engagementType" : "VideoContent1secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1284006965912494080",
              "tweetText" : "Niemand kann die Zukunft vorhersagen. Doch mit Derivaten kann man darauf wetten.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Forbes"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-07-31 18:43:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-07-31 18:45:54",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-07-31 18:46:03",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282662305923248129",
              "tweetText" : "🐎 Der Traum vom eigenen Pferd: Wie Ihr ihn Euch erfüllt und welche Kosten auf Euch zukommen:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 12:42:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:29:46",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1286607550868344832",
              "tweetText" : "Mit einer Säule 3a kannst du jährlich Steuern sparen. Gleichzeitig tust du was für deine Altersvorsorge.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCNews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nytimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCWorld"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CNN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCBreaking"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Sports news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Business news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Political News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entertainment news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "BBC News"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 12:42:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:30:12",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-08-01 19:30:11",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-01 19:30:10",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 19:30:17",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:30:11",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-01 19:30:09",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:30:13",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-01 19:30:10",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-08-01 19:30:12",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1283784015409971205",
              "tweetText" : "CHF 20.–/Mt. Rabatt auf Swiss mobile flat.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@derspiegel"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesschau"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NetflixDE"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ZDF"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@zeitonline"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SZ"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tech"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "smartphones"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "smartphone"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 12:42:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:29:06",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:29:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 19:29:47",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:29:08",
            "engagementType" : "VideoContent1secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282665929122500610",
              "tweetText" : "🌴☀ Einmal im Leben eine Weltreise machen! Was gilt es zu beachten bei Planung und Visa, Impfungen und Versicherungen, AHV und Pensionskasse? Wir erklären's:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 11:29:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 11:37:59",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288519058845908992",
              "tweetText" : "We built FaunaDB so you can focus on building your app instead of data infrastructure. See what others are saying!\n\nTry FaunaDB: https://t.co/48mbnD2Up2 https://t.co/v7gUcMMR4c",
              "urls" : [ "https://t.co/48mbnD2Up2" ],
              "mediaUrls" : [ "https://t.co/v7gUcMMR4c" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Fauna",
              "screenName" : "@fauna"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@reactjs"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@JavaScriptDaily"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@angular"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "zeit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "JAMstack"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-08-01 11:29:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 11:37:40",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 11:37:25",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-08-01 19:31:34",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-01 19:31:41",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 11:37:23",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1289427694581510144",
              "tweetText" : "Weshalb habt ihr ein #Freizügigkeitskonto eröffnet?",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Vorsorgewissen.info",
              "screenName" : "@vorsorgewissen"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 11:29:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 11:38:13",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282666263626551298",
              "tweetText" : "🏠  Ihr träumt von einer Ferienwohnung oder einem Ferienhaus am Lieblingsort? Wir zeigen, worauf es beim Kauf ankommt:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 22:10:43"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 22:10:45",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1273598258934415360",
              "tweetText" : "Kopf versus Bauch – das sind die 4 häufigsten psychologischen Fallen beim Anlegen:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Forbes"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 22:09:15"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 22:10:35",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 22:10:34",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-08-01 22:09:19",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 22:10:45",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 22:09:21",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-08-01 22:09:55",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-08-01 22:10:30",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-08-01 22:09:36",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 22:10:16",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-08-01 22:09:25",
            "engagementType" : "VideoContent6secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1286608038544191493",
              "tweetText" : "Lade die frankly App herunter und eröffne einfach und ohne Bankbesuch deine Säule 3a.\n#frankly #säule3a #privatevorsorge #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCNews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nytimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCWorld"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CNN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BBCBreaking"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Political News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "BBC News"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Business news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entertainment news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Sports news"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 18:21:17"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:16:40",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 19:16:46",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-01 19:16:39",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282661502651174912",
              "tweetText" : "⛵ Erst mit Mitte 60 in den Ruhestand? Das muss nicht sein. Wie der Traum von der Frühpensionierung wahr wird:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 18:58:30"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 18:58:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1282662087442006016",
              "tweetText" : "🐎 Der Traum vom eigenen Pferd: Wie Ihr ihn Euch erfüllt und welche Kosten auf Euch zukommen 👉 https://t.co/vma2Xea3Du https://t.co/PxsI6Ojxkq",
              "urls" : [ "https://t.co/vma2Xea3Du" ],
              "mediaUrls" : [ "https://t.co/PxsI6Ojxkq" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Basler Kantonalbank",
              "screenName" : "@BaslerKB"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 17:20:19"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 17:20:21",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1288051575504740354",
              "tweetText" : "Durch Homeschooling in Coronazeiten musste der Schulunterricht in kürzester Zeit digitalisiert werden. Viele Eltern haben so gemerkt, dass der Lerneffekt ihrer Kinder durch Smartphones und Tablets sogar steigen kann. Hat das herkömmliche Klassenzimmer ausgedient? https://t.co/U2wjIc1dtU",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/U2wjIc1dtU" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Medienkompetenzexperte",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "eltern"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "kids"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "mutter"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "education"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "vater"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "eduction"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "teaching"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "educations"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 19:39:23"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:39:25",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1167626743588503552",
              "tweetText" : "While enjoying any of our work, you are helping our next educational TV series. Thank You!",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Jean Mercier presents",
              "screenName" : "@apoemaday"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Comedy"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Comedy"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "teaching"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "education"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 and up"
            } ],
            "impressionTime" : "2020-08-01 19:11:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:12:13",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1289186143813042180",
              "tweetText" : "Popular #crypto token performance (24h) @OKEx: https://t.co/ah1EcaWlcs\n\n$BTC/USD: $11214.00 (+2.67%)\n$ETH/USD: $344.03 (+8.56%)\n$OKB/USD: $5.82 (+1.87%)\n$XRP/USD: $0.25 (+2.58%)\n$BCH/USD: $295.05 (+4.45%)\n$EOS/USD: $3.08 (+3.36%)",
              "urls" : [ "https://t.co/ah1EcaWlcs" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "OKEx",
              "screenName" : "@OKEx"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ExpansionMx"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@lajornadaonline"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Pajaropolitico"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@binance"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Bitcoin cryptocurrency"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Litecoin cryptocurrency"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Ethereum cryptocurrency"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Innovation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#innovation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "innovation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "disruption"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "BTC"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#Innovation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#disruption"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "btc"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#BTC"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-01 19:11:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-01 19:11:56",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-08-01 19:11:57",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-01 19:11:57",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-08-01 19:12:03",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1290208657267982336",
              "tweetText" : "Bist du #bereit? https://t.co/ihTSlFwysC",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/ihTSlFwysC" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-05 01:32:08"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-05 01:32:10",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-08-05 01:32:11",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-08-05 01:32:31",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1291253785965322243",
              "tweetText" : "Für unsere Kunden ist eine Immobilie das grösste Investment ihres Lebens. Deshalb bieten wir einen einmaligen, digitalen Service – inklusive persönlicher Beratung. #hausfinanzierung #hypothek #immobilienberatung https://t.co/bS12ScnIfV",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/bS12ScnIfV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Sparen"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-06 18:48:03"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-06 18:48:05",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1267812108739067907",
              "tweetText" : "Gut, dass Sie sich beim Anlegen auf die Expertentipps und die Risikoinformationen verlassen können. Vereinbaren Sie einen Beratungstermin.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bank CIC",
              "screenName" : "@Bank_CIC"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-06 23:38:38"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-06 23:38:40",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1291413625907814403",
              "tweetText" : "Join us next week on August 13th for our next TezTalks LIVE stream featuring Stephane De Baets of Elevated Returns. \n\nWe will also be giving away another #Tezos branded @ledger Nano S during the live stream. \n\nTune in for your chance to win!\n\nhttps://t.co/0JLvJwwrvP",
              "urls" : [ "https://t.co/0JLvJwwrvP" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Tezos Commons",
              "screenName" : "@TezosCommons"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-08-07 00:00:14"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-08-07 00:00:15",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
} ]